import React, { Component } from 'react';
import Icons from './components/icons/icons';
import Header from './components/header/header';
import BookmarkForm from './components/bookmark-form/bookmark-form';
import ResultsArea from './components/results-area/results-area';
import Modal from './components/modal/modal';
import DeleteWebsite from './components/delete-website/delete-website';
import EditWebsite from './components/edit-website/edit-website';
import Background from './components/background/background';
import './page.sass'

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isModalOpen: false,
      websiteDeleteId: '',
      isEditingWebsite: false,
      websites: [],
    };
  };

  componentDidMount = () => {
    this.getLocalStorage();
  };

  getLocalStorage = () => {
    const setWebsites = localStorage.getItem('websites');

    if (setWebsites !== null) {
      const websiteArray = setWebsites.split(',');

      this.setState({
          websites: websiteArray
      });
    }
  };

  setToLocalStorage = (value) => {
    if (value.length === 0) {
      localStorage.clear()
    } else {
      localStorage.setItem('websites', value);
    }
  };

  addWebsite = (newWebsite) => {
    let newWebsiteList = this.state.websites;

    newWebsiteList.push(newWebsite);
    this.updateWebsiteList(newWebsiteList);
  };

  updateWebsiteList = (value) => {
    this.setState({
        websites: value
    });

    this.setToLocalStorage(value);
  };

  deleteWebsite = (id) => {
    let newWebsiteList = this.state.websites.filter((website, index) => {
        return index !== id;
    });

    this.updateWebsiteList(newWebsiteList);
  };

  toggleModal = (websiteDeleteId) => {

    if (Number.isInteger(websiteDeleteId)) {
      this.setState({
        isModalOpen: !this.state.isModalOpen,
        websiteDeleteId: websiteDeleteId,
        isEditingWebsite: false
      });
    } else {
      this.setState({
        isModalOpen: !this.state.isModalOpen,
        isEditingWebsite: true
      });
    }
  }

  render() {
    return (
      <div className="page">
        <Modal show={this.state.isModalOpen} onClose={this.toggleModal}>
          {this.state.isEditingWebsite ? <EditWebsite addWebsite={this.addWebsite} /> : <DeleteWebsite deleteWebsite={this.deleteWebsite} websiteDeleteId={this.state.websiteDeleteId} onClose={this.toggleModal} />}
        </Modal>

        <main role="main" className="page__inner">
          <Icons />
          <Header />
          <BookmarkForm addWebsite={this.addWebsite} />
          <ResultsArea openModal={this.toggleModal} data={this.state.websites}/>
        </main>

        <Background />
      </div>
    )
  }
}

export default App;