let urlValidation = (input) => {
    /* eslint-disable */
    const websiteRegex = /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/;
    return websiteRegex.test(input);
};

export { urlValidation };
