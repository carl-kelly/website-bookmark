import React, { Component } from 'react';
import bgVideo from '../../assets/concert.mp4';
// import bgImg from './../assets/concert.jpg';
import './background.sass';

class Background extends Component {
  render() {
    return (
      <div className="bg">
        <div className="bg__media">
          {/*<img className="bg__img" src={bgImg} />*/}
          <video className="bg__video" muted autoPlay="autoplay" loop>
            <source src={bgVideo} type="video/mp4" />
            Your browser does not support HTML5 video.
          </video>
        </div>
      </div>
    );
  }
}

export default Background;
