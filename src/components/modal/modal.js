import React, { Component } from 'react';
import '../../utils/sass/button.sass'
import './modal.sass'

class Modal extends Component {

  render() {
    if(!this.props.show) {
      return null;
    }

    return (
      <div className="modal">
        <div className="modal__inner">
          <button className="modal__close" onClick={this.props.onClose}>
            Close
            <svg><use xlinkHref="#icon-delete"></use></svg>
          </button>
          
          {this.props.children}

        </div>
      </div>
    );
  }
}

export default Modal;
