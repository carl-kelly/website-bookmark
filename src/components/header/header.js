import React from 'react';
import './header.sass';

const Header = () => (
	<header role="banner" className="header">
		<svg className="header__brand"><use xlinkHref="#icon-logo"></use></svg>
	</header>
)

export default Header;
