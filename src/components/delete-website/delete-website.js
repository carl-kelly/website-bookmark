import React, { Component } from 'react';
import '../modal/modal.sass';
import '../../utils/sass/application.sass'

class DeleteWebsite extends Component {

  handleDelete = () => {
    this.props.deleteWebsite(this.props.websiteDeleteId);

    this.props.onClose();
  };

  render() {
    return (
    	<div>
    		<h2 className="modal__title">Are you sure you want to delete this?</h2>

        <button onClick={this.props.onClose} className="button button--dark">No thanks</button>

        <button onClick={this.handleDelete} className="button button--delete">Yes please</button>
    	</div>
    );
  }
}

export default DeleteWebsite;
