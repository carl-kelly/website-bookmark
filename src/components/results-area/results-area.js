import React, { Component } from 'react';
import WebsiteListing from '../website-listing/website-listing'
import NoBookmarks from '../no-bookmarks/no-bookmarks'
import './results-area.sass'

class ResultsArea extends Component {
  displayResults() {
    const areResults = this.props.data.length

    if (areResults > 0) {
      return <WebsiteListing openModal={this.props.openModal} data={this.props.data} />;
    } else {
      return <NoBookmarks />;
    }
  }

  render() {
    return (
      <div className="results-area">
        {this.displayResults()}
      </div>
    );
  }
}

export default ResultsArea;
