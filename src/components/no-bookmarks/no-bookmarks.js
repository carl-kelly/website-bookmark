import React from 'react';
import '../../utils/sass/container.sass'
import '../../utils/sass/button.sass'
import '../results-area/results-area.sass'

const NoBookmarks = () => {
	const focusInput = () => {
		let input = document.getElementById('bookmark-form-input');

    	input.focus()
    	input.placeholder = "Start typing..."
  };

  return (
    <div className="container">
      <h2 className="web-listing__title">You have no bookmarks yet!</h2>
      <button onClick={focusInput} className="button">Add one</button>
    </div>
  )
}

export default NoBookmarks;
