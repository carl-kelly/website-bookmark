import React, { Component } from 'react';
import WebsiteSingle from '../website-single/website-single'
import '../../utils/sass/container.sass'
import './website-listing.sass'

class WebsiteListing extends Component {
  renderItems() {
    return this.props.data.map((item, index) => (
      <WebsiteSingle openModal={this.props.openModal} id={index} key={index} item={item} />
    ))
  }

  render() {
    return (
        <div className="container">
            <h2 className="web-listing__title web-listing__title--left">Your bookmarks</h2>

            <ul className="web-listing__wrapper">
              {this.renderItems()}
            </ul>
        </div>
    );
  }
}

export default WebsiteListing;
