import React, { Component } from 'react';
import '../website-listing/website-listing.sass'
import './website.sass'

class WebsiteItem extends Component {

  handleEdit = () => {
    this.props.openModal();
  };

  handleDelete = () => {
    const deleteId = this.props.id

    this.props.openModal(deleteId);
  };

  render() {
    return (
      <li className="web-listing__item website">
        <img className="website__icon" src={"https://www.google.com/s2/favicons?domain=" + (this.props.item)} />

        <a className="website__title" target="_blank" href={'http://' + this.props.item}>
          {this.props.item}
        </a>

        <div className="website__actions">
          <button onClick={this.handleEdit} className="website__edit">
            edit
            <svg><use xlinkHref="#icon-edit"></use></svg>
          </button>
          <button onClick={this.handleDelete} className="website__delete">
            delete
            <svg><use xlinkHref="#icon-delete"></use></svg>
          </button>
        </div>
      </li>
    )
  }
}

export default WebsiteItem;
