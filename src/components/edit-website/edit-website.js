import React, { Component } from 'react';
import BookmarkForm from '../bookmark-form/bookmark-form';
import '../modal/modal.sass';
import '../../utils/sass/application.sass'

class DeleteWebsite extends Component {

  render() {
    return (
    	<div>
        <h2 className="modal__title">Edit your entry</h2>

    		<BookmarkForm addWebsite={this.props.addWebsite} />
    	</div>
    );
  }
}

export default DeleteWebsite;
