import React, { Component } from 'react';
import { urlValidation } from '../../utils/urlValidation';
import './bookmark-form.sass'

class BookmarkForm extends Component {
  constructor() {
    super();

    this.state = {
      websiteUrl: '',
      showError: false
    };
  };

  handleSubmit = (event) => {
    event.preventDefault();
    let inputValue = this.state.websiteUrl;

    if (urlValidation(inputValue)) {
      this.props.addWebsite(inputValue);

      this.setState({
        websiteUrl: '',
        showError: false
      });
    } else {
      this.setState({ showError: true });
    }
  };

  handleChange = (event) => {
    let input = event.target;

    this.setState({ websiteUrl: input.value });

    if (input.value) {
      input.classList.add("bookmark-form__input--filled");
      this.setState({
        showError: false
      });
    } else {
      input.classList.remove("bookmark-form__input--filled");
    }
  };

  render() {
    return (
      <form className="bookmark-form" onSubmit={this.handleSubmit}>
        <input id="bookmark-form-input" value={this.state.websiteUrl} className={"bookmark-form__input " + (this.state.showError ? 'bookmark-form__input--error' : '')} placeholder="Add a website to begin…" onChange={this.handleChange} />

        <button className="bookmark-form__submit">
          Submit
          <svg><use xlinkHref="#icon-add"></use></svg>
        </button>
      </form>
    );
  }
}

export default BookmarkForm;
